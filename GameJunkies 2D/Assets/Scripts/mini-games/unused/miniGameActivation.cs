using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class miniGameActivation : MonoBehaviour
{
    public GameObject miniGamePanel;
    public GameObject miniGame;
    public GameObject mainPlayerController;
    public bool sneak = false;

    private void Start()
    {
        miniGamePanel.SetActive(false); //deactivate window
        miniGame.SetActive(false); //set mini game render to false
        (mainPlayerController.GetComponent<PlayerController>()).enabled = true; //main game's character controller script is active on start
        (mainPlayerController.GetComponent<BoxCollider2D>()).enabled = true; //deactivate main game's character box collider
    }

    public void CloseMiniGame()
    {
        miniGamePanel.SetActive(false); //deactivate window
        (mainPlayerController.GetComponent<PlayerController>()).enabled = true; //activate main game's character controller script
        (mainPlayerController.GetComponent<BoxCollider2D>()).enabled = true; //deactivate main game's character box collider

        miniGame.SetActive(false); //set mini game render to false so that it's not active when next game is activated

        //list bool states of mini games here
        sneak = false;

    }

    public void ActivateMiniGame()
    {
        // if (sneak)
        // {
        miniGamePanel.SetActive(true); //activate window
        miniGame.SetActive(true); //activate render of sneak game
        (mainPlayerController.GetComponent<PlayerController>()).enabled = false; //deactivate main game's character controller script
        (mainPlayerController.GetComponent<BoxCollider2D>()).enabled = false; //deactivate main game's character box collider
        Time.timeScale = 0; //pause gameplay
        // }
    }
}