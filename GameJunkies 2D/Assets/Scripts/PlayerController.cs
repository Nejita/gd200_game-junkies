using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float xScale = 4f;
    [SerializeField] float yScale = 4f;
    [SerializeField] float nextLocationDistance = 3f;
    [SerializeField] Animator animator;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] bool walking;
    [SerializeField] bool moving;
    public Vector3 targetPosition;
    public static bool canControlPlayer = true;
    public LocationManager locationManager;

    //player start transform positions for each area
    public Transform southCityPlayerPos;
    public Transform centralCityPlayerPos;
    public Transform eastCityPlayerPos;
    public Transform westCityPlayerPos;
    public Transform pathFindingTargetPos;

    public static PlayerController instance;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        targetPosition = transform.position;
    }

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        Move();
    }

    public void Move()
    {
        if (canControlPlayer)
        {
            //set target position
            if (Input.GetMouseButtonDown(0)) //left mouse click
            {
                targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                targetPosition.z = transform.position.z; //sets z position to stay the same
                // targetPosition.y = transform.position.y; //sets y position to stay the same
                moving = true;
            }

            //move player
            if (moving & transform.position != targetPosition) //move to position if the current position and the last clicked position are different
            {
                float step = speed * Time.deltaTime; //regulates speed for a standard speed regardless of cpu power
                transform.position = Vector2.MoveTowards(transform.position, targetPosition, step);
                walking = true;
                animator.SetBool("IsWalking", walking);

                //change walk direction
                if (targetPosition.x > transform.position.x)
                {
                    transform.localScale = new Vector2(xScale, yScale); //character scale on x & y
                }
                else if (targetPosition.x < transform.position.x)
                {
                    transform.localScale = new Vector2(-xScale, yScale); //charcater scale on x & y; negative xScale flips right facing walk animation to left
                }
            }

            // don't move
            else if (transform.position == targetPosition)
            {
                StopMoving();
            }
        }

        //set player starting positions for each area
        if (LocationManager.southCityPos == true)
        {
            transform.position = southCityPlayerPos.position;
            targetPosition = transform.position;
            transform.position = transform.position;
            pathFindingTargetPos.position = southCityPlayerPos.position;
            LocationManager.southCityPos = false;
        }

        if (LocationManager.centralCityPos == true)
        {
            transform.position = centralCityPlayerPos.position;
            targetPosition = transform.position;
            transform.position = transform.position;
            
            LocationManager.centralCityPos = false;
        }

        if (LocationManager.eastCityPos == true)
        {
            transform.position = eastCityPlayerPos.position;
            targetPosition = transform.position;
            transform.position = transform.position;
            // pathFindingTargetPos.position = targetPosition;
            LocationManager.eastCityPos = false;
        }

        if (LocationManager.westCityPos == true)
        {
            transform.position = westCityPlayerPos.position;
            targetPosition = transform.position;
            transform.position = transform.position;
            LocationManager.westCityPos = false;
        }
    }

    public void StopMoving()
    {
        moving = false;
        walking = false;
        animator.SetBool("IsWalking", walking);
        targetPosition = transform.position;
        transform.position = transform.position;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        //Resets mouse click position to character position when colliding with blocking object    
        Debug.Log(("collided"));
        // if (targetPosition.x > transform.position.x)
        // {
        //    targetPosition.x = transform.position.x - 0.2f; 
        //    transform.position = transform.position;
        // }
        // if (targetPosition.x < transform.position.x)
        // {
        //    targetPosition.x = transform.position.x + 0.2f; 
        //    transform.position = transform.position;
        // }
        // if (targetPosition.y < transform.position.y)
        // {
        //    targetPosition.y = transform.position.y - 0.2f; 
        //    transform.position = transform.position;
        // }
        // if (targetPosition.y < transform.position.y)
        // {
        //    targetPosition.y = transform.position.y + 0.2f; 
        //    transform.position = transform.position;
        // }

        targetPosition = transform.position;
        transform.position = transform.position;

    }

    public void EnablePlayerControll()
    {
        if (canControlPlayer)
            canControlPlayer = false;
        else
            canControlPlayer = true;
    }

}