using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameMenu : MonoBehaviour
{
    public MiniGameManager mgManagerReference;  

    public GameObject mashMinigameButton; 

    public GameObject lockpickMinigameButton; 

    public GameObject closeMinigameMenuButton; 

    public void Start()
    {
        closeMinigameMenu(); 
    }

    public void openMinigameMenu()
    {
         
        mashMinigameButton.SetActive(true); 
        lockpickMinigameButton.SetActive(true); 
        closeMinigameMenuButton.SetActive(true);
        
 
    }

    public void closeMinigameMenu()
    {

        mashMinigameButton.SetActive(false); 
        lockpickMinigameButton.SetActive(false); 
        closeMinigameMenuButton.SetActive(false);
    }





}
