using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MiniGameManager : MonoBehaviour
{
    [Header("Unity Fields for minigame panels")]

    public GameObject sneakGamePanel; //mini game panel
    public GameObject mashGamePanel;
    public GameObject lockpickGamePanel;

    [Header("Unity Fields for scene elements")]

    public GameObject mainPlayerController;
    // public GameObject mainPlayer;
    public GameObject sceneButtons;
    public GameObject inventoryItems;
    // public GameObject sceneElements;
    // public GameObject sceneElementsDuringMinigames;

    public GameObject door;
    public GameObject doorConsole;

    private bool sneakGame = false;
    private bool mashGame = false;
    private bool lockpickGame = false;

    [Header("other")]
    public Button sneakActivator;

    [Header("Reset Sneak")]
    public Enemy sneakEnemyPos1;
    public Enemy sneakEnemyPos2;
    public Enemy sneakEnemyPos3;
    public Enemy sneakEnemyPos4;
    public Enemy sneakEnemyPos5;
    public Enemy sneakEnemyPos6;
    public Enemy sneakEnemyPos7;
    public Enemy sneakEnemyPos8;
    public Enemy sneakEnemyPos9;
    public Enemy sneakEnemyPos10;
    public DragMouseMove sneakPlayerPos;
    public Caught loseWindow;
    public ReachedEnd winWindow;
    public InteractClick interactClick;
    public LocationManager locationManager;
    public GameObject sneakRivalOfficeDialog;

    public void Start()
    {
        mainPlayerController.GetComponent<PlayerController>();
        sneakGamePanel.SetActive(false);
    }

    public void SneakGame()
    {
        mainPlayerController.GetComponent<PlayerController>().Move();
        StartCoroutine(ActivateSneak());
        sneakActivator.interactable = false;

    }

    public IEnumerator ActivateSneak()
    {
        yield return new WaitForSeconds(0.3f); //wait time from press to activation; this will activate even if player's not at target
        // if (Vector3.Distance(mainPlayerController.transform.position, mainPlayerController.GetComponent<PlayerController>().targetPosition) < +1.5f)//if player and target position are in float range by the time wait seconds ends, then activate minigame
        // { 
        sneakGame = true; //sneak mini game is active
        mainPlayerController.GetComponent<PlayerController>().enabled = false; //deactivate main game's character controller script
        sneakGamePanel.SetActive(true); //deactivate window
        // mainPlayer.SetActive(false); //deactivate character
        sceneButtons.SetActive(false); //deactivate character
        // sceneElements.SetActive(false); //deactivate scene elements like bg
        // sceneElementsDuringMinigames.SetActive(true); //activate bg element sprites

        sneakActivator.interactable = false;
        // }
    }

    public void ResetSneak()
    {
        //enemy positions
        sneakEnemyPos1.ResetPosition();
        sneakEnemyPos2.ResetPosition();
        sneakEnemyPos3.ResetPosition();
        sneakEnemyPos4.ResetPosition();
        sneakEnemyPos5.ResetPosition();
        sneakEnemyPos6.ResetPosition();
        sneakEnemyPos7.ResetPosition();
        sneakEnemyPos8.ResetPosition();
        sneakEnemyPos9.ResetPosition();
        sneakEnemyPos10.ResetPosition();
        //player 
        sneakPlayerPos.ResetPlayerPosition();
        //results
        loseWindow.ResetLose();
        winWindow.ResetWin();

    }

    public void MashGame()
    {
        mainPlayerController.GetComponent<PlayerController>().Move();
        StartCoroutine(ActivateMash());
        sneakActivator.interactable = false;

    }

    public IEnumerator ActivateMash()
    {
        yield return new WaitForSeconds(0.3f);
        mashGame = true;
        mainPlayerController.GetComponent<PlayerController>().enabled = false; //deactivate main game's character controller script
        mashGamePanel.SetActive(true);
        // mainPlayer.SetActive(false);
        sceneButtons.SetActive(false);
        // sceneElements.SetActive(false); //deactivate scene elements like bg
        // sceneElementsDuringMinigames.SetActive(true); //activate bg element sprites
        sneakActivator.interactable = false;

    }

    public void LockpickGame()
    {
        mainPlayerController.GetComponent<PlayerController>().Move();
        StartCoroutine(ActivateLockpick());
        sneakActivator.interactable = false;

    }

    public IEnumerator ActivateLockpick()
    {
        yield return new WaitForSeconds(0.3f);

        lockpickGame = true;
        mainPlayerController.GetComponent<PlayerController>().enabled = false; //deactivate main game's character controller script
        lockpickGamePanel.SetActive(true);
        // mainPlayer.SetActive(false);
        sceneButtons.SetActive(false);
        // sceneElements.SetActive(false); //deactivate scene elements like bg
        // sceneElementsDuringMinigames.SetActive(true); //activate bg element sprites
        sneakActivator.interactable = false;

    }

    public void CloseMiniGame()
    { //deactivate window
        mainPlayerController.GetComponent<PlayerController>().enabled = true; //activate main game's character controller script
        // mainPlayer.SetActive(true); //activate character
        sceneButtons.SetActive(true); //activate character
        sneakActivator.interactable = true;
        Time.timeScale = 1f;
        inventoryItems.SetActive(true);
        door.SetActive(true);
        doorConsole.SetActive(true);

        if (sneakGame == true) //if sneak mini game is true
        {
            // if (InteractClick.firstTimeRivalOffice == true && DragMouseMove.sneakSuccessful == true)
            // {
            //     sneakGame = false; //sneak mini game is not active 
            //     sneakGamePanel.SetActive(false);
            //     ResetSneak();
            //     sneakRivalOfficeDialog.GetComponent<NPCController>().ActivateDialogue();
            //     InteractClick.firstTimeRivalOffice = false;
            //     LocationManager.ending = true;
            // }
            sneakGame = false; //sneak mini game is not active 
            sneakGamePanel.SetActive(false);
            ResetSneak();
            // InteractClick.sneakGame = false;
            // sceneElements.SetActive(true); //deactivate scene elements like bg
            // sceneElementsDuringMinigames.SetActive(false); //activate bg element sprites
        }

        if (mashGame == true)
        {
            mashGame = false;
            mashGamePanel.SetActive(false);
            InteractClick.safeCrackGame = false;
            // sceneElements.SetActive(true); //deactivate scene elements like bg
            // sceneElementsDuringMinigames.SetActive(false); //activate bg element sprites
        }

        if (lockpickGame == true)
        {
            lockpickGame = false;
            InteractClick.mashGame = false;
            lockpickGamePanel.SetActive(false);
            // sceneElements.SetActive(true); //deactivate scene elements like bg
            // sceneElementsDuringMinigames.SetActive(false); //activate bg element sprites
        }

        //list below all the ifs for each mini game to close them (use the above if (sneak) as example)
    }

}