using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class ButtonmashScript : MonoBehaviour
{
    public GameObject minigame;  
    public GameObject VictoryUI; 
    public GameObject FailUI; 
    public GameObject StartButton;
    public GameObject MashButton;  
    private bool clockIsGoing; 
    private int playerMashCount = 0; 
    private int mashRequirement = 20; 

    private float timer = 10f;
    public Text timerText;  

    public void Start()
    {
        MashButton.SetActive(false); 
    }
    public void mashButton()
    {
        playerMashCount += 1; 
        if (playerMashCount >= mashRequirement)
        {
            VictoryUI.SetActive(true); 
            clockIsGoing = false; 
            MashButton.SetActive(false); 
        }
    }

    void Update() 
    {
        if (clockIsGoing == true)
        {
            timer -= Time.deltaTime; 
            timerText.text = timer.ToString("f0"); 
            if (timer < 0)
            {
                FailUI.SetActive(true);   
                failState();
                MashButton.SetActive(false); 
            }
        }
    }

    public void StartMinigame()
    {
        clockIsGoing = true; 
        StartButton.SetActive(false); 
        MashButton.SetActive(true); 
    }

    void failState()
    { 
        clockIsGoing = false; 
    }
}
