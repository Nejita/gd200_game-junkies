using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReachedEnd : MonoBehaviour
{
    [SerializeField] GameObject winUI;
    [SerializeField] GameObject resultBox;
    public GameObject sneakPlayer;
    public bool sneakSuccessful = false;
    public AudioClip winSound;
    public AudioSource win;
    public DragMouseMove mouseMove;

    private void Start()
    {
        winUI.SetActive(false);
        sneakPlayer.GetComponent<DragMouseMove>();
        win = GetComponent<AudioSource>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("You win");
            Time.timeScale = 0; //pause gameplay
            winUI.SetActive(true);
            resultBox.SetActive(true);
            DragMouseMove.sneakSuccessful = true;
            win.PlayOneShot(winSound);
        }
    }

    public void ResetWin()
    {
        winUI.SetActive(false);
        // DragMouseMove.sneakSuccessful = false;
    }
}