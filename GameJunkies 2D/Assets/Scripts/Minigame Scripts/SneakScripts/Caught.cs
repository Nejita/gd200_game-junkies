using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Caught : MonoBehaviour
{
    [SerializeField] GameObject loseUI;
    [SerializeField] GameObject resultBox;
    public GameObject sneakPlayer;
    public AudioClip caughtSound;
    public AudioClip mainSneakMusic;
    public AudioSource enemy;
    public DragMouseMove mouseMove;
    // public AudioSource main;

    private void Start()
    {
        loseUI.SetActive(false);
        sneakPlayer.GetComponent<DragMouseMove>();
        enemy = GetComponent<AudioSource>();

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("You lose");
            Time.timeScale = 0; //pause gameplay
            loseUI.SetActive(true);
            resultBox.SetActive(true);
            DragMouseMove.sneakSuccessful = false;
            enemy.PlayOneShot(caughtSound);
        }
    }

    public void ResetLose()
    {
        loseUI.SetActive(false);
        resultBox.SetActive(false);
        Time.timeScale = 1; //pause gameplay
        sneakPlayer.SetActive(true);
        // DragMouseMove.sneakSuccessful = false;
    }
}