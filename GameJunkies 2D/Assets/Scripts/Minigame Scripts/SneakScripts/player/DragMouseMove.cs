using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragMouseMove : MonoBehaviour
{
    public static bool sneakSuccessful = false;
    private Vector3 mousePosition;
    public Vector3 sneakStartPoint;
    private bool isMoving = false;
    [SerializeField] float moveSpeed = 4f;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] Camera mainCamera;

    private void Start()
    {
        mousePosition = transform.position;
        rb = GetComponent<Rigidbody2D>();
        sneakSuccessful = false;
        sneakStartPoint = transform.position;
    }

    private void Update()
    {

        if (Input.GetMouseButton(0)) //left mouse click
        {
            SetTargetPosition();
        }

        if (isMoving)
        {
            MovePlayer();
        }
    }

    void SetTargetPosition()
    {
        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = transform.position.z; //sets z position to stay the same

        isMoving = true;
    }

    void MovePlayer()
    {
        // transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePosition);
        transform.position = Vector3.MoveTowards(transform.position, mousePosition, moveSpeed * Time.deltaTime);

        if (transform.position == mousePosition)
        {
            isMoving = false;
        }
    }

    public void ResetPlayerPosition()
    {
        transform.position = sneakStartPoint;
        mousePosition = transform.position;
    }


    private void OnTriggerStay2D(Collider2D collision)//Resets mouse click position to character position when colliding with blocking object
    {
        Debug.Log(("collided"));
        mousePosition = transform.position;
        transform.position = transform.position;
    }

}