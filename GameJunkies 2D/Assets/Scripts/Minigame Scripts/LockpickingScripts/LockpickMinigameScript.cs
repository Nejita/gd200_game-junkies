using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LockpickMinigameScript : MonoBehaviour
{
    // Reference to the entire minigame, to be disabled upon failure. 
    public GameObject minigamePrefab;
    // This bool is used to track whether the rotate right button is being pressed or not
    public bool isRightButtonPressed = false;
    // This bool is used to track whether the rotate left button is being pressed or not
    public bool isLeftButtonPressed = false;
   
    public static bool isUnlockSuccessfull = false; //when this bool is true, the door can be clicked to open without doing minigame again
    public GameObject inventoryItems;//to turn inventory in scene back on
    public static bool leavingApartment = false; //leaving player apartment for first time
    
    public GameObject lockValve;
    public GameObject lockIndicator; 
    public GameObject lockIndicatorPosition1; 
    public GameObject lockIndicatorPosition2; 
    public GameObject lockIndicatorPosition3; 
    // The text for the current rotation value. 
    public Text currentRotValText;
    // The integer for the current rotation value. 
    private float currentRotVal;
    // The text for lock 1's value. 
    public Text lock1valText;
    // The integer for lock 1's value.  
    private float lock1val;
    // The text for lock 2's value. 
    public Text lock2valText;
    // The integer for lock 2's value.  
    private float lock2val;
    // The text for lock 3's value. 
    public Text lock3valText;
    // The integer for lock 3's value.  
    private float lock3val;

    private float minRotVal = -360f;
    private float maxRotVal = 360f;
    private float rotIncrement = 10f;

    private int currentLockID;

    public Image lock1;
    public Image lock2;
    public Image lock3;
    public AudioSource lockpickSound;
    public AudioSource lockpickSuccess;
    public AudioSource lockpickFinalSuccess;
    public AudioSource lockpickFail;
    public AudioSource lockpickFinalFail;


 
    void Start()
    {
        minigamePrefab.SetActive(true);
        currentRotVal = 0;
        currentRotValText.text = currentRotVal.ToString();
        currentLockID = 1;
        rngLock1Val();
        rngLock2Val();
        rngLock3Val();
        isLeftButtonPressed = false;
        isRightButtonPressed = false; 
        lock1valText.color = new Color32 (255, 0, 0, 100); 
    }

    public void onPointerDownRotateCurrentRotValRight()
    {
        isRightButtonPressed = true;
    }

    public void onPointerUpStopRotatingRight()
    {
        isRightButtonPressed = false;
    }

    public void onPointerDownRotateCurrentRotValLeft()
    {
        isLeftButtonPressed = true;
    }

    public void onPointerUpStopRotatingLeft()
    {
        isLeftButtonPressed = false;
    }

    void Update()
    {

        if (isLeftButtonPressed == true)
        {
            lockValve.transform.Rotate(Vector3.forward * 5);
            currentRotVal -= 5;
            if (currentRotVal <= -360)
            {
                currentRotVal = 0;
            }
            currentRotValText.text = currentRotVal.ToString();
            Debug.Log("Left Button is being pressed!");
        }
        else if (isLeftButtonPressed == false)
        {

        }

        if (isRightButtonPressed == true)
        {
            lockValve.transform.Rotate(Vector3.forward * -5);
            currentRotVal += 5;
            if (currentRotVal >= 360)
            {
                currentRotVal = 0;
            }

            currentRotValText.text = currentRotVal.ToString();
        }
        else if (isRightButtonPressed == false)
        {

        }

        switch (currentLockID)
        {
            case 1:
            lockIndicator.transform.position = lockIndicatorPosition1.transform.position; 
            break;

            case 2:
            lockIndicator.transform.position = lockIndicatorPosition2.transform.position; 
            break; 

            case 3:
            lockIndicator.transform.position = lockIndicatorPosition3.transform.position; 
            break; 

            case 0:
            lockIndicator.SetActive(false); 
            break; 
        }
        
        
        

    }

    // Below is the code for rotating the lock more precisely 

    public void slowLeftRotate()
    {
        lockValve.transform.Rotate(Vector3.forward * 1);
        currentRotVal -= 1;
        if (currentRotVal <= -360)
        {
            currentRotVal = 0;
        }
        currentRotValText.text = currentRotVal.ToString();
        lockpickSound.Play();
    }

    public void slowRightRotate()
    {
        lockValve.transform.Rotate(Vector3.forward * -1);
        currentRotVal += 1;
        if (currentRotVal >= 360)
        {
            currentRotVal = 0;
        }
        currentRotValText.text = currentRotVal.ToString();
        lockpickSound.Play();
    }

    public void enterLock()
    {
        switch (currentLockID)
        {
            case 1:

             

                if (currentRotVal == lock1val)
                {
                    successLock();
                    Debug.Log("Lock 1 bypassed!");
                    lock1.color = new Color32(0, 255, 0, 100);
                    lock1valText.color = new Color32 (0, 255, 0, 100); 
                    lockpickSuccess.Play();

                }
                else
                {
                    failLock();
                    rngLock1Val();
                    lockpickFinalFail.Play();
                     
                }

                break;

            case 2:

            

                if (currentRotVal == lock2val)
                {
                    successLock();
                    Debug.Log("Lock 2 bypassed!");
                    lock2.color = new Color32(0, 255, 0, 100);
                    lock2valText.color = new Color32(0, 255, 0, 100); 
                    lockpickSuccess.Play();
 
                }
                else
                {
                    failLock();
                    rngLock2Val();
                    lock1.color = new Color32(255, 0, 0, 100);
                    lock1valText.color = new Color32(255, 0, 0, 100); 
                    lock2valText.color = new Color32(255, 255, 255, 100); 
                    Debug.Log("Lock 2 botched. Resetting to Lock 1...");
                    lockpickFail.Play();

                }
                break;

            case 3:

             

                if (currentRotVal == lock3val)
                {
                    finalSuccessLock();
                    lock3.color = new Color32(0, 255, 0, 100);
                    lock3valText.color = new Color32(0, 255, 0, 100); 
                    lockpickFinalSuccess.Play();
 
                }
                else
                {
                    failLock();
                    rngLock3Val();
                    lock2.color = new Color32(255, 0, 0, 100);
                    lock3valText.color = new Color32(255, 255, 255, 100); 
                    lock2valText.color = new Color32(255, 0, 0, 100); 
                    Debug.Log("Lock 3 botched. Resetting to Lock 2...");
                    lockpickFail.Play();
 
                }
                break;

            case 0:
                failLock();
                break;
        }

    }

    // Lock Success and Fail conditions below

    public void successLock()
    {
        currentLockID += 1;
    }

    public void finalSuccessLock()
    {
        // Insert any narrative success scripts here or something
        Debug.Log("LOCKPICKING SUCCESSFUL!!!!");
        minigamePrefab.SetActive(false);
        isUnlockSuccessfull = true;
        leavingApartment = true;
        inventoryItems.SetActive(true);
    }

    public void failLock()
    {
        currentLockID -= 1;
        if (currentLockID == 0)
        {
            minigamePrefab.SetActive(false);
            Debug.Log("Lockpicking Failed...");
        }
    }

    public void rngLock1Val()
    {
        float randomNumber = Random.Range(minRotVal, maxRotVal);
        float numSteps = Mathf.Floor(randomNumber / rotIncrement);
        lock1val = numSteps * rotIncrement;
        lock1valText.text = lock1val.ToString();
    }

    public void rngLock2Val()
    {
        float randomNumber = Random.Range(minRotVal, maxRotVal);
        float numSteps = Mathf.Floor(randomNumber / rotIncrement);
        lock2val = numSteps * rotIncrement;
        lock2valText.text = lock2val.ToString();
    }

    public void rngLock3Val()
    {
        float randomNumber = Random.Range(minRotVal, maxRotVal);
        float numSteps = Mathf.Floor(randomNumber / rotIncrement);
        lock3val = numSteps * rotIncrement;
        lock3valText.text = lock3val.ToString();
    }
}