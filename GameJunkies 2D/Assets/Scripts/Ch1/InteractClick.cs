using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractClick : MonoBehaviour
{
    public static bool sneakGame = false;
    public static bool safeCrackGame = false;
    public static bool mashGame = false;

    public static bool npcBartender = false;
    public static bool npcDesk = false;
    public static bool npcFather = false;
    public static bool branchNPC = false;

    public static bool joesBar = false;
    public static bool enteringBar = false;
    public static bool leavingOffice = false;
    public static bool parentsOffice = false;
    public static bool siblingApartment = false;
    public static bool yourApartment = false;
    public static bool moonlightComplexBuilding = false;

    public static bool tvInteract = true;
    public static bool tvCanInteract = true;
    public static bool tvButtons = false;
    public static bool lockInteract = false;

    public static bool firstTimeOffice = true;
    public static bool firstTimeLeavingOffice = true;
    public static bool firstTimeLeavingOfficeDialog = false;
    public static bool firstTimeSiblingApartment = true;
    public static bool firstTimeRivalOffice = true;
    public static bool firstTimeLeavingRivalOffice = true;
    public static bool spokeToJoe = false;

    public static bool ending = false;

    public GameObject centralCityOpening;

    public GameObject gameManager;
    public GameObject inventoryItems;
    public GameObject _npcBartender;
    public GameObject player;
    public GameObject door;
    public GameObject doorConsole;
    public GameObject sceneButtons;
    public GameObject sneakOptions;
    public GameObject sneakDialogOption;



    public LockpickMinigameScript isUnlockSuccessfull;

    public Color startColour;
    public Color canClickColour = Color.gray;
    SpriteRenderer clickableObject;

    private void Start()
    {
        clickableObject = GetComponent<SpriteRenderer>();
        tvCanInteract = true;
        tvInteract = false;
        sneakOptions.SetActive(false);
        firstTimeOffice = true;
        firstTimeLeavingOffice = true;
        firstTimeLeavingOfficeDialog = false;
        firstTimeSiblingApartment = true;
        firstTimeRivalOffice = true;
        firstTimeLeavingRivalOffice = true;
        
        ending = false;
    }

    private void Update()
    {
        if (tvButtons) //button options for tv
        {
            tvInteract = true;
            tvCanInteract = false;
            sneakOptions.SetActive(true);
        }

    }

    public void LockPick() //called in dialog holder script
    {
        inventoryItems.SetActive(false);
        safeCrackGame = true;
        Debug.Log("safe clicked");
        gameManager.GetComponent<MiniGameManager>().LockpickGame();
        sneakOptions.SetActive(false);
        tvInteract = false;
        tvCanInteract = false;
        lockInteract = false;
    }

    public void OnMouseDown()
    {
        if (gameObject.tag == "Tv")
        {
            tvInteract = true;
            tvCanInteract = false;
            gameObject.GetComponent<NPCController>().ActivateDialogue();
        }

        if (gameObject.tag == "Deactivate choice")
        {
            tvInteract = false;
            tvCanInteract = true;
            tvButtons = false;
            sneakOptions.SetActive(false);
        }

        //minigame clicks
        if (gameObject.tag == "Sneak")
        {

            inventoryItems.SetActive(false);
            sneakGame = true;
            gameManager.GetComponent<MiniGameManager>().SneakGame();
            door.SetActive(false);
            doorConsole.SetActive(false);
            sneakOptions.SetActive(false);
            tvInteract = false;
            tvCanInteract = false;
            tvButtons = false;
            sneakDialogOption.SetActive(false);

            Debug.Log("sneak clicked");
        }

        if (gameObject.tag == "SafeCrack")
        {
            lockInteract = true;
            gameObject.GetComponent<NPCController>().ActivateDialogue();
        }

        if (gameObject.tag == "Mash")
        {
            inventoryItems.SetActive(false);
            mashGame = true;
            Debug.Log("mash clicked");
        }

        //npc clicks
        if (gameObject.tag == "npcBartender")
        {
            npcBartender = true;
            Debug.Log("npcBartender clicked");
            gameObject.GetComponent<NPCController>().ActivateDialogue();
            spokeToJoe = true;
        }

        if (gameObject.tag == "npcFather")
        {
            npcFather = true;
            Debug.Log("father clicked");
            gameObject.GetComponent<NPCController>().ActivateDialogue();
            leavingOffice = true;
        }

        if (gameObject.tag == "npcDesk")
        {
            npcDesk = true;
            Debug.Log("desk clicked");
            gameObject.GetComponent<NPCController>().ActivateDialogue();
        }

        if (gameObject.tag == "BranchNPC")
        {
            branchNPC = true;
            Debug.Log("BranchNPC clicked");
            gameObject.GetComponent<NPCController>().ActivateDialogue();
        }

        // Location clicks

        if (gameObject.tag == "MoonlightComplex")
        {
           moonlightComplexBuilding = true;
           Debug.Log("complex clicked");
        }
        
        if (gameObject.tag == "ending")
        {
          ending = true;
          gameObject.GetComponent<NPCController>().ActivateDialogue();
          gameObject.SetActive(false);
        }

        if (gameObject.tag == "goesToCentralCity")
        {
            if (LockpickMinigameScript.isUnlockSuccessfull == true)
            {
                gameManager.GetComponent<LocationManager>().GoToCentralCity();
            }
            if (LockpickMinigameScript.leavingApartment == true)
            {
                centralCityOpening.GetComponent<NPCController>().ActivateDialogue();
                LockpickMinigameScript.leavingApartment = false;

                if (LockpickMinigameScript.leavingApartment == false)
                {
                    player.GetComponent<PlayerController>().enabled = true;
                    sceneButtons.SetActive(true); //activate character
                }
            }

            Debug.Log("Central City clicked");
        }

        if (gameObject.tag == "goesToWestCity")
        {
            gameManager.GetComponent<LocationManager>().GoToWestCity();
        }

        if (gameObject.tag == "goesToEastCity")
        {
            gameManager.GetComponent<LocationManager>().GoToEastCity();
        }

        if (gameObject.tag == "goesToSouthCity")
        {
            gameManager.GetComponent<LocationManager>().GoToSouthCity();
        }

        if (gameObject.tag == "goesToJoesBar")
        {
            joesBar = true;
            enteringBar = true;
        }
        if (gameObject.tag == "goesToParentsOffice")
        {
            parentsOffice = true;
        }


        if (gameObject.tag == "goesToYourApt.")
        {
            yourApartment = true;
        }

        if (gameObject.tag == "goesToSiblingApt.")
        {
            gameManager.GetComponent<LocationManager>().GoToSiblingApartment();
        }
        

        if (gameObject.tag == "goesToRivalOffice")
        {
            gameManager.GetComponent<LocationManager>().GoToRivalOffice();
        
        }
    }

    private void OnMouseOver()
    {
        if (gameObject.tag != "Untagged" || gameObject.tag != "Player")
        {
            clickableObject.color = canClickColour;

            if (LockpickMinigameScript.isUnlockSuccessfull == true)
            {
                if (gameObject.tag == "goesToCentralCity")
                {
                    clickableObject.color = canClickColour;
                }
            }
            else if (LockpickMinigameScript.isUnlockSuccessfull == false)
            {
                if (gameObject.tag == "goesToCentralCity")
                {
                    clickableObject.color = Color.white;
                }
            }

            if (tvCanInteract == true)
            {
                if (gameObject.tag == "Tv")
                {
                    clickableObject.color = canClickColour;
                }
            }
            else if (tvCanInteract == false)
            {
                if (gameObject.tag == "tv")
                {
                    clickableObject.color = Color.white;
                }
            }
        }

    }

    private void OnMouseExit()
    {
        // mirrorDialogueOptions.SetActive(false);
        clickableObject.color = Color.white;
        if (gameObject.tag == "goesToCentralCity")
        {
            clickableObject.color = Color.white;
        }
    }
}