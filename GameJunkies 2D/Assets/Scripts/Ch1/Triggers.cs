using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggers : MonoBehaviour
{
    public GameObject speechBlip;
    public GameObject mirrorDialogueOptions;
    public GameObject interactPrompt;
    public GameObject gameManager;
    public GameObject barOpening;
    public InteractClick click;
    public GameObject complexLocations;
    public GameObject rivalPlace;

    public Animation interactPromptAnimator;
    // Start is called before the first frame update
    void Start()
    {
        speechBlip.SetActive(false);
        interactPromptAnimator = interactPrompt.GetComponent<Animation>();
        interactPromptAnimator.Play();
        gameManager.GetComponent<MiniGameManager>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "npcBartender" && InteractClick.npcBartender == true) //|| collision.gameObject.tag == "BranchNPC"
        {

            speechBlip.SetActive(true);
            collision.gameObject.GetComponent<NPCController>().ActivateDialogue();
        }

        if (collision.gameObject.tag == "BranchNPC" && InteractClick.branchNPC)
        {
            speechBlip.SetActive(true);
            collision.gameObject.GetComponent<NPCController>().ActivateDialogue();
            mirrorDialogueOptions.SetActive(true);
        }

        // if (collision.gameObject.tag == "Sneak" && InteractClick.sneakGame == true) //if collides with something tagged sneak and sneak is true then run sneak game
        // {
        //     speechBlip.SetActive(true);
        //     gameManager.GetComponent<MiniGameManager>().SneakGame();
        //     InteractClick.sneakGame = false;
        //     Debug.Log("Sneak Game bool set back to false");
        // }

        // if (collision.gameObject.tag == "SafeCrack" && InteractClick.safeCrackGame == true) //if collides with something tagged sneak and sneak is true then run sneak game
        // {
        //     speechBlip.SetActive(true);
        //     gameManager.GetComponent<MiniGameManager>().LockpickGame();
        //     InteractClick.sneakGame = false;
        //     Debug.Log("Sneak Game bool set back to false");
        // }

        if (collision.gameObject.tag == "Mash" && InteractClick.mashGame == true) //if collides with something tagged sneak and sneak is true then run sneak game
        {
            speechBlip.SetActive(true);
            gameManager.GetComponent<MiniGameManager>().MashGame();
            InteractClick.sneakGame = false;
            Debug.Log("Sneak Game bool set back to false");
        }

        if (collision.gameObject.tag == "goesToJoesBar" && InteractClick.joesBar == true) //if collides with something tagged sneak and sneak is true then run sneak game
        {
            gameManager.GetComponent<LocationManager>().GoToJoesBar();
            InteractClick.joesBar = false;

            if (InteractClick.enteringBar == true)
            {
                barOpening.GetComponent<NPCController>().ActivateDialogue();
                InteractClick.enteringBar = false;
            }
        }

        if (collision.gameObject.tag == "goesToParentsOffice" && InteractClick.parentsOffice == true) //if collides with something tagged sneak and sneak is true then run sneak game
        {
            gameManager.GetComponent<LocationManager>().GoToParentsOffice();
            InteractClick.parentsOffice = false;
        }

        if (collision.gameObject.tag == "goesToSiblingApt." && InteractClick.siblingApartment == true) //if collides with something tagged sneak and sneak is true then run sneak game
        {
            gameManager.GetComponent<LocationManager>().GoToSiblingApartment();
            InteractClick.siblingApartment = false;
        }

        if (collision.gameObject.tag == "goesToYourApt." && InteractClick.yourApartment == true) //if collides with something tagged sneak and sneak is true then run sneak game
        {
            gameManager.GetComponent<LocationManager>().GoToYourApartment();
            InteractClick.yourApartment = false;
        }

        if (collision.gameObject.tag == "MoonlightComplex" &&  InteractClick.moonlightComplexBuilding == true)
        {
            complexLocations.SetActive(true);
            // InteractClick.spokeToJoe = true;//remove, this is for testing only

            if (InteractClick.spokeToJoe == true)
            {
                rivalPlace.SetActive(true);
            }
            else if(InteractClick.spokeToJoe == false)
            {
                rivalPlace.SetActive(false);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "npcBartender" || collision.gameObject.tag == "BranchNPC")
        {

            speechBlip.SetActive(false);
            mirrorDialogueOptions.SetActive(false);
        }

        if (collision.gameObject.tag == "Sneak")
        {
            speechBlip.SetActive(false);
            InteractClick.sneakGame = false;
            Debug.Log("Sneak Game bool set back to false");
        }
        if (collision.gameObject.tag == "SafeCrack")
        {
            speechBlip.SetActive(false);
            InteractClick.safeCrackGame = false;
            Debug.Log("safe crack Game bool set back to false");
        }
        if (collision.gameObject.tag == "Mash")
        {
            speechBlip.SetActive(false);
            InteractClick.mashGame = false;
            Debug.Log("mash Game bool set back to false");
        }

        if (collision.gameObject.tag == "MoonlightComplex")
        {
            complexLocations.SetActive(false);
        }
    }
}