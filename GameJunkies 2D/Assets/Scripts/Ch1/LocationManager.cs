using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour
{
    [Header("Script Refs")]
    public GameObject mainPlayer;
    public GameObject leavingOfficeDialog;
    public GameObject enterRivalOfficeDialog;

    public GameObject leavingSiblingApartmentDialog;
    public GameObject openingGameDialog;
    public GameObject openingOfficeDialog;
    public GameObject openingSiblingApartmentDialog;

    public InteractClick click;
    public PickUp pickUp;
    public static bool ending = false;

    [Header("Locations")]

    public GameObject yourApartment;
    public GameObject siblingApartment;
    public GameObject rivalOffice;
    public GameObject sneakBtn;
    public GameObject parentsOffice;
    public GameObject joesBar;
    public GameObject centralCity;
    public GameObject westCity;
    public GameObject eastCity;
    public GameObject southCity;

    [Header("Player Spawn positions")]
    public static bool centralCityPos = false;
    public static bool eastCityPos = false;
    public static bool westCityPos = false;

    //player positions for locations
    public static bool southCityPos = false;

    [Header("Audio, SFX and Music Management")]

    public AudioSource MusicPlayerForYourApartment;
    public AudioSource MusicPlayerForSiblingApartment;
    public AudioSource MusicPlayerForParentsOffice;
    public AudioSource MusicPlayerForRivalOffice;
    public AudioSource MusicPlayerForJoesBar;
    public AudioSource AmbiencePlayerForCentralCity;

    void Start()
    {
        yourApartment.SetActive(true); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForYourApartment.Play(); 
        mainPlayer.SetActive(false); //deactivate character
        openingGameDialog.GetComponent<NPCController>().ActivateDialogue();
        ending = false;
    }

    public void GoToYourApartment()
    {
        yourApartment.SetActive(true); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForYourApartment.Play();
        AmbiencePlayerForCentralCity.Stop();
        mainPlayer.SetActive(false); //deactivate character
    }

    public void GoToSiblingApartment()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(true); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForSiblingApartment.Play();
        AmbiencePlayerForCentralCity.Stop();
        mainPlayer.SetActive(false); //deactivate character

        mainPlayer.GetComponent<Triggers>().complexLocations.SetActive(false);
        mainPlayer.GetComponent<Triggers>().rivalPlace.SetActive(false);

        if (InteractClick.firstTimeSiblingApartment == true) //opening dialog for office
        {
            openingSiblingApartmentDialog.GetComponent<NPCController>().ActivateDialogue();
            InteractClick.firstTimeOffice = false;
        }
    }

    public void GoToRivalOffice()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(true); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForRivalOffice.Play();
        AmbiencePlayerForCentralCity.Stop();
        mainPlayer.SetActive(false); //deactivate character
        mainPlayer.GetComponent<Triggers>().complexLocations.SetActive(false);
        mainPlayer.GetComponent<Triggers>().rivalPlace.SetActive(false);

        if (InteractClick.firstTimeRivalOffice == true)
        {
            enterRivalOfficeDialog.GetComponent<NPCController>().ActivateDialogue();
            sneakBtn.SetActive(true);
            InteractClick.firstTimeRivalOffice = false;
        }
        else if (InteractClick.firstTimeRivalOffice == false)
        {
            sneakBtn.SetActive(false);
        }
    }

    public void GoToParentsOffice()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(true); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForParentsOffice.Play();
        AmbiencePlayerForCentralCity.Stop();
        mainPlayer.SetActive(false); //deactivate character

        if (InteractClick.firstTimeOffice == true) //opening dialog for office
        {
            openingOfficeDialog.GetComponent<NPCController>().ActivateDialogue();
            sneakBtn.SetActive(true);
            InteractClick.firstTimeLeavingOfficeDialog = true;
        }

    }

    public void GoToJoesBar()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(true); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForJoesBar.Play();
        AmbiencePlayerForCentralCity.Stop();
        mainPlayer.SetActive(false); //deactivate character
    }

    public void GoToCentralCity()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(true); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForYourApartment.Stop();
        AmbiencePlayerForCentralCity.Play();
        mainPlayer.SetActive(true); //activate character
        mainPlayer.GetComponent<PlayerController>().StopMoving();
        centralCityPos = true; //set player east city start position to true

    }

    public void GoToWestCity()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(true); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForJoesBar.Stop();
        AmbiencePlayerForCentralCity.Play();
        mainPlayer.SetActive(true); //activate character
        mainPlayer.GetComponent<PlayerController>().StopMoving();
        westCityPos = true; //set player west city start position to true

    }

    public void GoToEastCity()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(true); //set as inactive location
        southCity.SetActive(false); //set as inactive location
        MusicPlayerForSiblingApartment.Stop();
        AmbiencePlayerForCentralCity.Play();
        mainPlayer.SetActive(true); //activate character
        mainPlayer.GetComponent<PlayerController>().StopMoving();
        eastCityPos = true; //set player east city start position to true
        Debug.Log("go east");

        if (PickUp.foundTab == true)
        {
            leavingSiblingApartmentDialog.GetComponent<NPCController>().ActivateDialogue();
            PickUp.foundTab = false;
        }

    }

    public void GoToSouthCity()
    {
        yourApartment.SetActive(false); //set as active location
        siblingApartment.SetActive(false); //set as inactive location
        rivalOffice.SetActive(false); //set as inactive location
        parentsOffice.SetActive(false); //set as inactive location
        joesBar.SetActive(false); //set as inactive location
        centralCity.SetActive(false); //set as inactive location
        westCity.SetActive(false); //set as inactive location
        eastCity.SetActive(false); //set as inactive location
        southCity.SetActive(true); //set as inactive location
        MusicPlayerForParentsOffice.Stop();
        AmbiencePlayerForCentralCity.Play();
        mainPlayer.SetActive(true); //activate character
        mainPlayer.GetComponent<PlayerController>().StopMoving();
        southCityPos = true; //set player south city start position to true
        

        if (InteractClick.firstTimeLeavingOffice == true && InteractClick.firstTimeLeavingOfficeDialog == true)
        {
            leavingOfficeDialog.GetComponent<NPCController>().ActivateDialogue();
            InteractClick.firstTimeLeavingOffice = false;
        }
    }

}