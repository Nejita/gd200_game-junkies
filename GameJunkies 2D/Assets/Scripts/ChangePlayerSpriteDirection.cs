using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlayerSpriteDirection : MonoBehaviour
{
    public PlayerController playerController;
    [SerializeField] float xScale = 4f;
    [SerializeField] float yScale = 4f;
    // Update is called once per frame
    void Update()
    {
        //change walk direction
        if (playerController.targetPosition.x > playerController.transform.position.x)
        {
            transform.localScale = new Vector2(xScale, yScale); //character scale on x & y
        }
        else if (playerController.targetPosition.x < playerController.transform.position.x)
        {
            transform.localScale = new Vector2(-xScale, yScale); //charcater scale on x & y; negative xScale flips right facing walk animation to left
        }
    }
}