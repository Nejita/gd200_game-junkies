using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public GameObject itemButton;
    private NewInventory inventory;
    public GameObject playerRef;
    public static bool foundTab = false;


    public Color canClickColour = Color.gray;
    SpriteRenderer clickableObject;

    private void Start()
    {
        inventory = playerRef.GetComponent<NewInventory>();
        clickableObject = GetComponent<SpriteRenderer>();

    }

    private void OnMouseDown()
    {
        if (gameObject.tag == "KeyCard" || gameObject.tag == "Receipt")
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (inventory.isFull [i] == false)
                {
                    //Item can be added to the inventory
                    inventory.isFull [i] = true;
                    Instantiate(itemButton, inventory.slots [i].transform, false);
                    Destroy(gameObject);
                    break;
                }
            }
        }

        if (gameObject.tag == "Receipt" )
        {
            gameObject.GetComponent<NPCController>().ActivateDialogue();
            foundTab = true;
        }
    }

    private void OnMouseOver()
    {
        if (gameObject.tag != "Untagged" || gameObject.tag != "Player")
        {
            clickableObject.color = canClickColour;
        }

    }

    private void OnMouseExit()
    {
        clickableObject.color = Color.white;
    }
}