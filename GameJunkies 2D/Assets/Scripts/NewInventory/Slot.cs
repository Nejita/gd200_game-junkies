using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    private NewInventory _inventory;
    public GameObject playerRef;
    public int i;

    private void Start() {
       _inventory = playerRef.GetComponent<NewInventory>();
    }

    private void Update() {
        if(transform.childCount <= 0){
           _inventory.isFull[i] = false;
        }
    }
    public void DropItem(){
       foreach(Transform child in transform){
           GameObject.Destroy(child.gameObject);
       }
    }
}
