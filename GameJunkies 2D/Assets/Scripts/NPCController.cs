using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
   [SerializeField]private GameObject dialogue;

   public void ActivateDialogue()
   {
       dialogue.SetActive(true); 
   }

//return if the dialogue is currently active or not
   public bool DialogueActive(){
       return dialogue.activeInHierarchy;
   }
}
