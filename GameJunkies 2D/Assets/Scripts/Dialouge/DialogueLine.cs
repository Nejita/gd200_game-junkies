using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace DialogueSystem{
    public class DialogueLine : DialogueBaseClass
{
  [Header("Text Options")]
  [SerializeField] private string input;
  [SerializeField]private Color textColor;
 [SerializeField] private Font textFont;
 
 [Header("Time Parameters")]
 [SerializeField] private float delay;
 [SerializeField] private float delayBetweenLines;
 
 [Header("Sound")]
 [SerializeField] private AudioClip sound;
 
 [Header("Character Image")]
 [SerializeField] private Sprite CharacterSprite;
 [SerializeField] private Image imageHolder;

 public PlayerController playerScript;//player script ref to disable movement

 private IEnumerator lineAppear;
  private Text textHolder;

  private void Awake() 
  {  
    //image for the player and npc when talking
    imageHolder.sprite = CharacterSprite;
    imageHolder.preserveAspect = true;
    PlayerController.canControlPlayer = false;
  }

  private void OnEnable() 
  {
    ResetLine();
    lineAppear = (WriteText(input, textHolder, textColor, textFont, delay, sound, delayBetweenLines));
    //Initializes the dialogue
     StartCoroutine(lineAppear);
  }

  private void Update() 
  {
    if (Input.GetMouseButtonDown(1))
    {
      //when input is detected show entire text without any delay
      if(textHolder.text != input)
      {
        StopCoroutine(lineAppear);
        textHolder.text = input;

      }
      else
          finished = true;
    }
  }
  
  //Resets text holder
  private void ResetLine()
  {
     textHolder = GetComponent<Text>();
      textHolder.text = "";
      finished = false;
  }
  
}

}
