using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace DialogueSystem
{
    public class DialogueHolder : MonoBehaviour
    {
        private IEnumerator dialogueSeq;
        public PlayerController playerScript; //player script ref to disable movement
        public GameObject buttonManger; //player script ref to disable movement
        public GameObject interactScriptObj; //player script ref to disable movement
        public InteractClick interactClick; //interaction for choices after dialog runs

        private void OnEnable()
        {
            dialogueSeq = dialogueSequence();
            StartCoroutine(dialogueSeq);
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                //deactivates holder and the object
                Deactivate();
                gameObject.SetActive(false);
                StopCoroutine(dialogueSeq);
                PlayerController.canControlPlayer = true;
            }
        }
        private IEnumerator dialogueSequence()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Deactivate();
                transform.GetChild(i).gameObject.SetActive(true);
                yield return new WaitUntil(() => transform.GetChild(i).GetComponent<DialogueLine>().finished);
            }
            gameObject.SetActive(false);
            PlayerController.canControlPlayer = true;//turn off controller movement
            //call tv options after dialog runs
            if (InteractClick.tvInteract == true)
            {
                InteractClick.tvButtons = true;
            }

            //call lock pick after dialog
            if (InteractClick.lockInteract == true)
            {
                interactScriptObj.GetComponent<InteractClick>().LockPick();
            }

            if (InteractClick.ending == true)
            {
                buttonManger.GetComponent<ButtonsManager>().GameOver();
            }

            

        }

        //Deactivates the other children so that there is no overlaping between dialogue lines
        private void Deactivate()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}