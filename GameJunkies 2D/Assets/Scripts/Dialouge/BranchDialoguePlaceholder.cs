using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchDialoguePlaceholder : MonoBehaviour
{
   public Triggers playerREF; 
   void HideChoices()
   {
       playerREF.mirrorDialogueOptions.SetActive(false); 
   }  

}
