using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace DialogueSystem
{
    public class DialogueBaseClass : MonoBehaviour
    {
        //Indicates when line is finished or not
        public bool finished { get; protected set; }
        //Coroutine shows player the text one letter at a time with delays
        //Includes the colour, text font, text, audio and delay
        protected IEnumerator WriteText(string input, Text textHolder, Color textColor, Font textFont, float delay, AudioClip sound, float delayBetweenLines)
        {
            textHolder.color = textColor;
            textHolder.font = textFont;

            for (int i = 0; i < input.Length; i++)
            {
                textHolder.text += input [i];
                //plays sound for each letter

                yield return new WaitForSeconds(delay);
            }
            yield return new WaitUntil(() => Input.GetMouseButton(0));
            finished = true;
        }
    }
}