using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour
{
    public GameObject pauseUI;
    public GameObject mainPlayerController;
    public Button controlButton;
    public AudioSource musicPlayerForMainMenu; 
    public AudioSource musicPlayerForEnd; 

    public void PlayGame()
    {
        SceneManager.LoadScene("Ch1");
        musicPlayerForMainMenu.Stop(); 
    }

    public void GameOver()//this is the end of the game
    {
        SceneManager.LoadScene("end");

    }

    public void ControlsMenu()
    {
        SceneManager.LoadScene("ControlsMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Game has quit");
    }
    public void ExitToMenu()
    {
        SceneManager.LoadScene("MainMenu");
        musicPlayerForMainMenu.Play(); 
    }

    public void PauseGame()
    {
        // Pause Menu
        pauseUI.SetActive(true);
        Time.timeScale = 0f;
        // mainPlayerController.GetComponent<PlayerController>().enabled = false; //activate main game's character controller script
        mainPlayerController.GetComponent<PlayerController>().StopMoving();
        controlButton.interactable = true;
        
    }

    public void ResumeGame()
    {
        pauseUI.SetActive(false);
        Time.timeScale = 1f;
        // mainPlayerController.GetComponent<PlayerController>().enabled = true; //deactivate main game's character controller script
        mainPlayerController.GetComponent<PlayerController>().StopMoving();
         
    }

}