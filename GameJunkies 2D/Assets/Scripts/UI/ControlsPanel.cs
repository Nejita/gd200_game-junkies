using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsPanel : MonoBehaviour
{
    [SerializeField] GameObject mainControl;
    [SerializeField] GameObject lockControl;
    [SerializeField] GameObject sneakControl;
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject controlsMenu;

    public void ControlsWindow()
    {
        controlsMenu.SetActive(true);
        MainControls();
    }

    public void MainControls()
    {
        mainControl.SetActive(true);
        lockControl.SetActive(false);
        sneakControl.SetActive(false);
        pauseMenu.SetActive(false);
    }

    public void LockpickGameControls()
    {
        mainControl.SetActive(false);
        lockControl.SetActive(true);
        sneakControl.SetActive(false);
        pauseMenu.SetActive(false);
    }

    public void SneakGameControls()
    {
        mainControl.SetActive(false);
        lockControl.SetActive(false);
        sneakControl.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void PauseMenuPanel()
    {
        mainControl.SetActive(false);
        lockControl.SetActive(false);
        sneakControl.SetActive(false);
        pauseMenu.SetActive(true);
        controlsMenu.SetActive(false);
    }
}