using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SuccessBehaviour : MonoBehaviour
{

    public GameObject Door;
    public LockpickMinigameScript isUnlockSuccessfull;

    void Start()
    {
        Door.GetComponent<InteractClick>();
    }

    // Update is called once per frame
    void Update()
    {
        if(LockpickMinigameScript.isUnlockSuccessfull == true)
        {
            Door.GetComponent<InteractClick>().enabled = true;
        }
    }

}
