// using System.Collections;
// using System.Collections.Generic;
// using System;
// using UnityEngine;
// using UnityEditor.UI;
// using UnityEditor.Experimental.GraphView;
// using UnityEditor;
// using UnityEngine.UIElements;
// public class DialogueGraphView : GraphView 
// {

//     private readonly Vector2 defaultNodeSize = new Vector2(150, 200);
//    public DialogueGraphView(){
//        styleSheets.Add(Resources.Load<StyleSheet>("DialogueGraph"));
//        //allows to zoom in and out
//        SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);

//        this.AddManipulator(new ContentDragger());
//        this.AddManipulator(new SelectionDragger());
//        this.AddManipulator(new RectangleSelector());

//        var grid = new GridBackground();
//        Insert(0, grid);
//        grid.StretchToParentSize();

//        AddElement(GenerateEntryPointNode());
//    }

//    public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
//    {
//        var compatiblePorts = new List<Port>();

//        ports.ForEach((port) => 
//        {
//            if(startPort != port && startPort.node != port.node)
//            compatiblePorts.Add(port);
//        });
//        return compatiblePorts;
//    }

//    private Port GeneratePort(DialogueNode node, Direction portDirection, Port.Capacity capacity = Port.Capacity.Single){
//       return node.InstantiatePort(Orientation.Horizontal, portDirection, capacity, typeof(float));
//    }

//     internal DialogueNode CreateDialogueNode(string nodeName)
//     {
//         var dialogueNode = new DialogueNode{
//            title = nodeName,
//            DialogueText = nodeName,
//            GUID = Guid.NewGuid().ToString()
//         };

//         var inputPort = GeneratePort(dialogueNode, Direction.Input, Port.Capacity.Multi);
//         inputPort.portName = "Input";
//         dialogueNode.inputContainer.Add(inputPort);

//         var button = new Button(()=>{AddChoicePort(dialogueNode);});
//         button.text = "New Choice";
//         dialogueNode.titleContainer.Add(button);

//         dialogueNode.RefreshExpandedState();
//         dialogueNode.RefreshPorts();
//         dialogueNode.SetPosition(new Rect(Vector2.zero, defaultNodeSize));

//         return dialogueNode;
//     }

//     private DialogueNode GenerateEntryPointNode()
//    {
//        var node = new DialogueNode
//        {
//           title = "START",
//           GUID = Guid.NewGuid().ToString(),
//           DialogueText = "ENTRYPOINT",
//           EntryPoint = true
//        };

//        var generatedPort = GeneratePort(node, Direction.Output);
//        generatedPort.portName = "NEXT";
//        node.outputContainer.Add(generatedPort);

//        node.RefreshExpandedState();
//        node.RefreshPorts();

//        node.SetPosition(new Rect(100, 200, width: 100, height: 150));

//        return node;
//    }

//    public void CreateNode(string nodeName){
//        AddElement(CreateDialogueNode(nodeName));
//    }

//    private void AddChoicePort(DialogueNode dialogueNode)
//    {
//        var generatedPort = GeneratePort(dialogueNode,Direction.Output);

//        var outputPortCount = dialogueNode.outputContainer.Query("connector").ToList().Count;
//        generatedPort.portName = $"choice {outputPortCount}";

//        dialogueNode.outputContainer.Add(generatedPort);
//        dialogueNode.RefreshPorts();
//        dialogueNode.RefreshExpandedState();
//    }
// }
