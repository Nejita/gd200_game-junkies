using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Choices : MonoBehaviour
{
   public GameObject textBox;
   public GameObject choice01;
   public GameObject choice03;
   public GameObject choice02;
   public int choiceMade;

   public void ChoiceOption1()
   {
       textBox.GetComponent<Text>().text = "Wow you actually made a choice";
       choiceMade = 1;
   }

   public void ChoiceOption2()
   {
       textBox.GetComponent<Text>().text = "How can i implement this into my dialogue system?";
       choiceMade = 2;
   }

    public void ChoiceOption3()
   {
       textBox.GetComponent<Text>().text = "Paulo please help me figure this out!";
       choiceMade = 3;
   }

    // Update is called once per frame
    void Update()
    {
        // if not zero hide the other choices
        if(choiceMade >= 1){
            choice01.SetActive(false);
            choice02.SetActive(false);
            choice03.SetActive(false);
        }
    }
}
