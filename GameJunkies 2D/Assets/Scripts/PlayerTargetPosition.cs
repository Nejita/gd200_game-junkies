using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargetPosition : MonoBehaviour
{
    public PlayerController playerScript;

    void Start()
    {
        transform.position = playerScript.targetPosition;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = playerScript.targetPosition;
    }
}